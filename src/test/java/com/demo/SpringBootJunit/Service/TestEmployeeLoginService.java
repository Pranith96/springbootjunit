package com.demo.SpringBootJunit.Service;

import static org.junit.Assert.assertEquals;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import com.demo.SpringBootJunit.Entity.User;
import com.demo.SpringBootJunit.Repository.EmployeeLoginRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestEmployeeLoginService {

	@Autowired
	EmployeeLoginService employeeLoginService;

	@MockBean
	EmployeeLoginRepository repository;

	@Test
	public void getUserTest() {

		Mockito.when(repository.findAll()).thenReturn(
				Stream.of(new User(1, "pranith", "hcl"), new User(2, "pavan", "hcl")).collect(Collectors.toList()));

		assertEquals(2, employeeLoginService.findAllUser().size());
	}

}
