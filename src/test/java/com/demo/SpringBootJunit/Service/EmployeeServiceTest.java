package com.demo.SpringBootJunit.Service;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.demo.SpringBootJunit.Entity.User;
import com.demo.SpringBootJunit.Repository.EmployeeRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeServiceTest {
	
	@Autowired
	EmployeeService employeeService;

	@MockBean
	EmployeeRepository employeeRepository;
	
	@Test
	public void testSaveUser() {
		User user = new User();
		user.setName("pranith");
		user.setCompanyName("hcl");
		Mockito.when(employeeRepository.save(user)).thenReturn(user);
		assertEquals(user, employeeService.addEmployee(user));
	}
	
	
}
