package com.demo.SpringBootJunit.Controller;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.demo.SpringBootJunit.Entity.User;
import com.demo.SpringBootJunit.Service.EmployeeServiceById;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration

public class TestEmployeeControllerById {

	private MockMvc mockMvc;

	@MockBean
	EmployeeServiceById employeeServiceById;

	@Autowired
	private WebApplicationContext context;

	ObjectMapper objectMapper = new ObjectMapper();

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
	}

	@Test
	public void findByIdTest() throws Exception {

		/*
		 * * List<User> users = new ArrayList<>(); String jsonRequest =
		 * objectMapper.writeValueAsString(users); MvcResult result = mockMvc
		 * .perform(get("/Id").param("id",
		 * "1").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
		 * .andExpect(status().isOk()).andReturn();
		 * 
		 * String resultContent = result.getResponse().getContentAsString();
		 * UserResponse response = objectMapper.readValue(resultContent,
		 * UserResponse.class); Assert.assertTrue(response.isStatus() == Boolean.TRUE);
		 */

		 List<User> users = Arrays.asList(
		            new User(1, "pranith","hcl"),
		            new User(2, "Pavan","hcl"));

		String jsonRequest = objectMapper.writeValueAsString(users);

		Mockito.when(employeeServiceById.findById(1)).thenReturn(users);
	
		mockMvc.perform(get("/user/{id}",1)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[0].id", is(1))).andExpect(jsonPath("$.[0].name", is("pranith")))
				.andExpect(jsonPath("$.[0].companyName", is("hcl")));
		Mockito.verify(employeeServiceById,Mockito.times(1)).findById(1);
		Mockito.verifyNoMoreInteractions(employeeServiceById);

	}
	
	/*@Test
	public void test_get_by_id_fail_404_not_found() throws Exception {
		Mockito.when(employeeServiceById.findById(1)).thenReturn(null);
	    mockMvc.perform(get("/get/{id}", 1))
	            .andExpect(status().isNotFound());
	    Mockito.verify(employeeServiceById, Mockito.times(1)).findById(1);
	    Mockito.verifyNoMoreInteractions(employeeServiceById);
	}*/

	/*
	 * @Test public void findByIdTest() throws Exception {
	 * mockMvc.perform(MockMvcRequestBuilders.get("/Id/{id}",1).accept(MediaType.
	 * APPLICATION_JSON)) .andExpect(jsonPath("$.id").exists())
	 * .andExpect(jsonPath("$.name").exists())
	 * .andExpect(jsonPath("$.companyName").exists())
	 * .andExpect(jsonPath("$.id").value(1))
	 * .andExpect(jsonPath("$.name").value("pranith"))
	 * .andExpect(jsonPath("$.companyName").value("hcl")).andDo(
	 * MockMvcResultHandlers.print()); }
	 */

	/*
	 * @Test public void test_get_by_id_fail_404_not_found() throws Exception {
	 * 
	 * Mockito.when(employeeServiceById.findById(1)).thenReturn(null);
	 * 
	 * mockMvc.perform(get("/Id/{id}", 1)).andExpect(status().isNotFound());
	 * 
	 * }
	 */

}
