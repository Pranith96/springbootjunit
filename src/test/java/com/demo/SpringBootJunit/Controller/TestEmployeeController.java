package com.demo.SpringBootJunit.Controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.demo.SpringBootJunit.Entity.User;
import com.demo.SpringBootJunit.Service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
// @ContextConfiguration(classes= EmployeeController.class)

public class TestEmployeeController {

	
	private MockMvc mockMvc;

	@MockBean
	EmployeeService employeeService;

	@Autowired
	private WebApplicationContext context;

	ObjectMapper objectMapper = new ObjectMapper();

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
	}

	
	//SAve data
	@Test
	public void addEmployeeTest() throws Exception {
		User user = new User();

		user.setName("lokesh");
		user.setCompanyName("HCL");
		/*
		 * String jsonRequest = objectMapper.writeValueAsString(user); 
		 * MvcResult result= mockMvc.perform(post("/save").content(jsonRequest).contentType(MediaType.
		 * APPLICATION_JSON)) .andExpect(status().isOk()).andReturn();
		 * 
		 * String resultContent = result.getResponse().getContentAsString();
		 * UserResponse response = objectMapper.readValue(resultContent,
		 * UserResponse.class); Assert.assertTrue(response.isStatus() == Boolean.TRUE);
		 */

		String jsonRequest = objectMapper.writeValueAsString(user);
		Mockito.when(employeeService.addEmployee(user)).thenReturn(user);
		//Mockito.doNothing().when(employeeService);

		mockMvc.perform(post("/save").contentType(MediaType.APPLICATION_JSON).content(jsonRequest))
				.andExpect(status().isOk()).andReturn();
		
//		Mockito.verify(employeeService,Mockito.times(1)).addEmployee(user);
//		Mockito.verifyNoMoreInteractions(employeeService);

	}

}
