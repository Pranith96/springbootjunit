package com.demo.SpringBootJunit.Controller;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.demo.SpringBootJunit.Entity.User;
import com.demo.SpringBootJunit.Service.EmployeeLoginService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestEmployeeLoginController {

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext context;

	@MockBean
	EmployeeLoginService employeeLoginService;

	ObjectMapper objectMapper = new ObjectMapper();

	@Before
	public void setup() {
		
		this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();

	}

	@Test

	public void findAllUserTest() throws Exception {

		// To get response as output we go for below process
		/*
		 * List<User> user = new ArrayList<>(); String jsonRequest =
		 * objectMapper.writeValueAsString(user); MvcResult result =
		 * mockMvc.perform(get("/login").content(jsonRequest).contentType(MediaType.
		 * APPLICATION_JSON)) .andExpect(status().isOk()).andReturn();
		 * 
		 * String resultContent = result.getResponse().getContentAsString();
		 * UserResponse response = objectMapper.readValue(resultContent,
		 * UserResponse.class); Assert.assertTrue(response.isStatus() == Boolean.TRUE);
		 */

		List<User> user = Arrays.asList(new User(1, "Prem", "Hcl"), new User(2, "rahul", "CTS"));
		Mockito.when(employeeLoginService.findAllUser()).thenReturn(user);

		mockMvc.perform(get("/login")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$", hasSize(2)))
				.andExpect(jsonPath("$[0].id", is(1)))
				.andExpect(jsonPath("$[0].name", is("Prem")))
				.andExpect(jsonPath("$[0].companyName", is("Hcl")))
				.andExpect(jsonPath("$[1].id", is(2)))
				.andExpect(jsonPath("$[1].name", is("rahul")))
				.andExpect(jsonPath("$[1].companyName", is("CTS")));
		
		Mockito.verify(employeeLoginService, Mockito.times(1)).findAllUser();
		Mockito.verifyNoMoreInteractions(employeeLoginService);

	}
}
