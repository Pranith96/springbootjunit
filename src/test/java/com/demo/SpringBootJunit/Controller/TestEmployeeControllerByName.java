package com.demo.SpringBootJunit.Controller;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.demo.SpringBootJunit.Entity.User;
import com.demo.SpringBootJunit.Service.EmployeeServiceByName;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration

public class TestEmployeeControllerByName {
	
	private MockMvc mockMvc;
	
	@MockBean
	EmployeeServiceByName employeeServiceByName;

	@Autowired
	private WebApplicationContext context;

	ObjectMapper objectMapper = new ObjectMapper();

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
	}

	@Test
	 public void testFindByName() throws Exception {
		 List<User> users = Arrays.asList(
		            new User(1, "pranith","hcl"),
		            new User(2, "Pavan","hcl"));

		 String jsonRequest = objectMapper.writeValueAsString(users);

			Mockito.when(employeeServiceByName.findByName("pranith")).thenReturn(users);

			mockMvc.perform(get("/name/{name}","pranith")).andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
					.andExpect(jsonPath("$.[0].id", is(1))).andExpect(jsonPath("$.[0].name", is("pranith")))
					.andExpect(jsonPath("$.[0].companyName", is("hcl")));
			Mockito.verify(employeeServiceByName,Mockito.times(1)).findByName("pranith");
			Mockito.verifyNoMoreInteractions(employeeServiceByName);

		}
		
	
}

