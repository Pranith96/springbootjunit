package com.demo.SpringBootJunit.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.demo.SpringBootJunit.Entity.User;
import com.demo.SpringBootJunit.ResponseModel.UserResponse;
import com.demo.SpringBootJunit.Service.EmployeeServiceById;

@RestController
public class EmployeeControllerById {

	@Autowired
	EmployeeServiceById employeeServiceById;

	@GetMapping(value="/user/{id}")
	
	public List<User> findById(@PathVariable("id") Integer id) {

		List<User> users = employeeServiceById.findById(id);
		UserResponse userResponse = new UserResponse();
		userResponse.setMessage("success");
		userResponse.setStatus(true);
		return users;

	}

}
