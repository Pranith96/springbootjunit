package com.demo.SpringBootJunit.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.demo.SpringBootJunit.Entity.User;
import com.demo.SpringBootJunit.ResponseModel.UserResponse;
import com.demo.SpringBootJunit.Service.EmployeeServiceByName;

@RestController
public class EmployeeControllerByName {
	
	@Autowired
	EmployeeServiceByName employeeServiceByName;
	
	@GetMapping(value="/name/{name}")
	
	public List<User> findByName(@PathVariable("name") String name) {
		
		List<User> users = employeeServiceByName.findByName(name);
		UserResponse userResponse = new UserResponse();
		userResponse.setMessage("success");
		userResponse.setStatus(true);
		return users;
		
	}
	
}
