package com.demo.SpringBootJunit.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.SpringBootJunit.Entity.User;
import com.demo.SpringBootJunit.ResponseModel.UserResponse;
import com.demo.SpringBootJunit.Service.EmployeeLoginService;

@RestController
public class EmployeeLoginController {

	@Autowired
	EmployeeLoginService employeeLoginService;

	@GetMapping("/login")
	public List<User> findAllUser() {

		List<User> responseUser = employeeLoginService.findAllUser();
		UserResponse userResponse= new UserResponse();
		userResponse.setMessage("method got success");
		userResponse.setStatus(true);
		String str=userResponse.toString();
		System.out.println(str);
		return responseUser;

	}
}
