package com.demo.SpringBootJunit.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.SpringBootJunit.Entity.User;
import com.demo.SpringBootJunit.ResponseModel.UserResponse;
import com.demo.SpringBootJunit.Service.EmployeeService;

@RestController
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;

	@PostMapping("/save")

	public UserResponse addEmployee(@RequestBody User user) {

		User responseUser = employeeService.addEmployee(user);

		UserResponse userResponse = new UserResponse();
		userResponse.setMessage("Success");
		userResponse.setStatus(true);

		return userResponse;

	}

}
