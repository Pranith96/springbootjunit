package com.demo.SpringBootJunit.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.SpringBootJunit.Entity.User;

@Repository
public interface EmployeeRepositoryByName extends JpaRepository<User, String> {

	List<User> findByName(String name);

}
