package com.demo.SpringBootJunit.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.SpringBootJunit.Entity.User;

@Repository
public interface EmployeeRepositoryById extends JpaRepository<User, Integer> {

	List<User> findById(int id);

}
