package com.demo.SpringBootJunit.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class User {

	@Id
	@GeneratedValue
	private int id;
	private String name;
	private String companyName;

	
	public User() {
	
	}

	
	public User(int id, String name, String companyName) {
		this.id=id;
		this.name = name;
		this.companyName = companyName;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", companyName=" + companyName + "]";
	}
	
	

}
