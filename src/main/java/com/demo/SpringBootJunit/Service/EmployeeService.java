package com.demo.SpringBootJunit.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.SpringBootJunit.Entity.User;
import com.demo.SpringBootJunit.Repository.EmployeeRepository;

@Service
public class EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;

	public User addEmployee(User user) {
		return employeeRepository.save(user);
	}

}
