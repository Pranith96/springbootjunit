package com.demo.SpringBootJunit.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.SpringBootJunit.Entity.User;
import com.demo.SpringBootJunit.Repository.EmployeeLoginRepository;


@Service
public class EmployeeLoginService {
	
	@Autowired
	EmployeeLoginRepository employeeLoginRepository;	


	public List<User> findAllUser() {
		// TODO Auto-generated method stub
		
		List<User> users=employeeLoginRepository.findAll();
		
		System.out.println(users);
		return users;
	}

}
