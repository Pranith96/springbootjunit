package com.demo.SpringBootJunit.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.SpringBootJunit.Entity.User;
import com.demo.SpringBootJunit.Repository.EmployeeRepositoryById;

@Service
public class EmployeeServiceById {

	@Autowired
	EmployeeRepositoryById employeeRepositoryById;

	public List<User> findById(int id) {
		return employeeRepositoryById.findById(id);
	}

}
