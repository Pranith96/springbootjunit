package com.demo.SpringBootJunit.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.SpringBootJunit.Entity.User;
import com.demo.SpringBootJunit.Repository.EmployeeRepositoryByName;

@Service
public class EmployeeServiceByName {

	@Autowired
	EmployeeRepositoryByName employeeRepositoryByName;
	
	public List<User> findByName(String name) {
		return employeeRepositoryByName.findByName(name) ;
	}

}
